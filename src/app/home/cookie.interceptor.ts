import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuthService } from './auth.service';

@Injectable()
export class CookieInterceptor implements HttpInterceptor {

    constructor(
        private readonly auth: AuthService,
    ) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        const cookie = this.auth.getCookie();

        // attach a Cookie
        if (cookie) {
            request = request.clone({
                setHeaders: {
                    Cookie: this.auth.getCookie()
                }
            });
        }

        // capture a new token if it is attached to the header
        return next.handle(request).pipe(
            tap(event => {
                if (event instanceof HttpResponse) {
                    const cookie = event.headers.get('set-cookie');
                    console.log("----- cookie in interceptor ----\n", cookie);
                    if (cookie)
                        this.auth.setCookie(cookie);
                }
            })
        )
    }
}

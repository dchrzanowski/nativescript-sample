import { Component, OnInit, ViewChild } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { EventData } from "tns-core-modules/ui/page/page";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from "./auth.service";

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {

    input: { email: string, password: string } = {
        email: '',
        password: ''
    };
    data: string = "Some text";

    constructor(
        private readonly http: HttpClient,
        private readonly auth: AuthService,
    ) { }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onButtonTap(args: EventData) {
        if (this.auth.getCookie()) {
            console.log("----- current cookie ----\n", this.auth.getCookie());
        }
    }

    onLogin(args: EventData) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
        this.data = this.input.email + ", " + this.input.password;
        this.http.post('http://10.0.2.2:8080/login-mobile', this.input, httpOptions)
            .subscribe((data: any) => {
                console.log("----- data from post login ----\n", data);
            })
    }

    onRequestAuth(args: EventData) {
        this.http.get('http://10.0.2.2:8080/profile-mobile')
            .subscribe((data: any) => {
                this.data = JSON.stringify(data, null, 2);
                console.log("----- data from request auth ----\n", data);
            });
    }

    onRequestNonAuth(args: EventData) {
        this.http.get('http://10.0.2.2:8080/mobile-info')
            .subscribe((data: any) => {
                this.data = JSON.stringify(data, null, 2);
                console.log("----- data from request non-auth ----\n", data);
            });
    }
}

import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { NativeScriptFormsModule } from 'nativescript-angular/forms';
import { NativeScriptHttpClientModule } from 'nativescript-angular/http-client';

import { HomeRoutingModule } from "./home-routing.module";
import { HomeComponent } from "./home.component";
import { CookieInterceptor } from "./cookie.interceptor";
import { AuthService } from "./auth.service";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        HomeRoutingModule,
        NativeScriptHttpClientModule
    ],
    declarations: [
        HomeComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ],
    providers: [
        // {
        //     provide: HTTP_INTERCEPTORS,
        //     useClass: CookieInterceptor,
        //     multi: true
        // },
        AuthService
    ]
})
export class HomeModule { }

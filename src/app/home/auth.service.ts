import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {
    cookie: string = '';
    constructor() {

    }

    getCookie() {
        if (!this.cookie)
            return null;
        else
            return this.cookie;
    }

    setCookie(cookie: string) {
        if (cookie.includes('connect.sid'))
            this.cookie = cookie.split(';').shift();
    }
}
